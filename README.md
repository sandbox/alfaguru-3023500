# Simple circuit breaker implementation for Drupal

## Introduction

See https://www.martinfowler.com/bliki/CircuitBreaker.html for an explanation of the circuit breaker concept.

This module provides a simple interface to integrate a circuit breaker with external services.

## Creating circuit breaker configurations

Use the administration interface at Administration -> Configuration -> Web services -> Circuit breakers to define circuit breaker 
configurations. Typically you will need one for each service to be integrated so you might have one for Salesforce, one for Shipwire
and so on.

The algorithm for the circuit breaker uses the analogy of a thermal trip. There
is no attempt to mimic the actual behaviour of such a device: this is simply intended as a conceptual model
to help users understand how the software works.

In this model the trip monitors the 'temperature' of the circuit. Each failure event raises the temperature by some amount whereas each success lowers it. The device also cools with time,
so after a while the temperature returns to normal by itself in the absence of further events. 

If the trip threshold temperature is reached then the circuit breaks and the service is cut off. 
After a certain time it will switch into a test (reset) mode. There is a probabilistic element to the reset control. 
This is so that if multiple requests occur simultaneously the likelihood is that  
only one will enter test mode at that time, avoiding a stampede which might otherwise occur. 

For simplicity the normal (floor) temperature is considered to be zero while the trip temperature is 100 degrees. Other parameters
are all configurable as follows:

* The increment for a failure event F is a non-zero value between 1 and 100 degrees.
* The decrement for a success event S is a value between zero and 100 degrees.
* The "cooling rate" R can be set between 10 degrees per hour (slow) and 10 degrees per second (rapid).
* The time T1 after which the trip _may_ reset can be set to any number of seconds.     
* The time T2 after which the trip _will_ reset can be set to any number of seconds equal to or higher than T1.

Note that when the circuit resets its temperature is still high (at 100 - S) so it will trip again rapidly
in the event of further failures.

In code you can prevent or permit resets dynamically or force the circuit breaker to be open or closed. 
Specifically you might permit resets only during cron runs for maximum control.   

## Recommended configurations

There are two recommended configurations:

1. Retry via cron. If cron is run frequently enough, then you can use a cron hook to do all retries of your service. You'll need to set a suitable fixed retry interval (both values the same) and set retry as not allowed in all other places the circuit breaker is called.
1. Randomised retry. Set a first retry interval appropriate to the needs of your service and set the second one at a higher value depending on typical traffic (more traffic, larger value).

## Code example

```php

function callMyService($service, $args) {
  $cb = \Drupal::service('circuit_breaker.factory')->load('my_service');
  // If circuit is broken, don't retry.
  $cb->setRetryAllowed(FALSE);
  try {
    return $cb->execute(function($args) use($service) {
       $service->invoke($args);
    }, $args, MyAllowableException::class);
  }
  catch(MyAllowableException $applicationError) {
    // handle application error
  }
  catch(Throwable $exception) {
    // handle other exceptions
  }
```

Note that the code which invokes your service should always throw an exception when there is a network or other failure for which the circuit breaker may be tripped.

