<?php

namespace Drupal\circuit_breaker\Storage;

/**
 * Persistent storage for a circuit breaker.
 */
interface StorageInterface {

  /**
   * Get the last recorded temperature.
   *
   * @return int
   *   The last recorded temperature.
   */
  public function getTemperature();

  /**
   * Set a new temperature value.
   *
   * Before storing, should adjust the value so it is an integer between 0 and 100.
   *
   * @param int $t
   *  The new temperature.
   */
  public function setTemperature($t);

  /**
   * Timestamp of last recorded event.
   *
   * @return int
   *   Last record time in seconds since epoch.
   */
  public function getLastUpdateTime();

  /**
   * Timestamp of last recorded failure.
   *
   * @return int
   *   Last failure time in seconds since epoch.
   */
  public function getLastFailureTime();

  /**
   * Set timestamp of last recorded failure.
   *
   * @param int
   *   Last failure time in seconds since epoch.
   */
  public function setLastFailureTime($time);

  /**
   * Record a failure event (typically an exception).
   *
   * The storage implementation must store the time of the
   * event and keep a counter. It may also log it.
   *
   * @param object $object
   *   The failure event.
   */
  public function recordFailure($object);

  /**
   * Is the circuit broken?
   *
   * @return bool
   *   The current state of the circuit.
   */
  public function isBroken();

  /**
   * Set the state of the circuit.
   *
   * @param bool $state
   *   The new state value.
   */
  public function setBroken($state);

  /**
   * Save all changes to the persistent store.
   *
   * The time must be recorded for retrieval as the last update time.
   */
  public function persist();

}
