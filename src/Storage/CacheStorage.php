<?php

namespace Drupal\circuit_breaker\Storage;

use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Persistent state for a circuit breaker using Drupal's cache.
 */
class CacheStorage implements StorageInterface {

  /**
   * The circuit breaker ID.
   *
   * @var string
   */
  protected $key;

  /**
   * Cache data.
   *
   * @var object
   */
  protected $data;

  /**
   * The cache storage interface.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * CacheStorage constructor.
   *
   * @param string $key
   *   The circuit breaker ID.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   Cache storage.
   * @param object $data
   *   Current data (if any).
   */
  public function __construct($key, CacheBackendInterface $cacheBackend, $data) {
    $this->key = $key;
    $this->data = $data ? $data->data : $this->defaultData();
    $this->cacheBackend = $cacheBackend;
  }

  /**
   * {@inheritdoc}
   */
  protected function defaultData() {
    $data = new \stdClass();
    $data->failureCount = 0;
    $data->lastFailureTime = 0;
    $data->lastUpdateTime = 0;
    $data->isBroken = FALSE;
    $data->temperature = 0;
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getTemperature() {
    return $this->data->temperature;
  }

  /**
   * {@inheritdoc}
   */
  public function setTemperature($t) {
    $t = (int)max(0, min(100, $t));
    $this->data->temperature = $t;
  }

  /**
   * {@inheritdoc}
   */
  public function getLastUpdateTime() {
    return $this->data->lastUpdateTime;
  }

  /**
   * {@inheritdoc}
   */
  public function recordFailure($object) {
    $this->data->failureCount++;
    $this->data->lastFailureTime = time();
  }

  /**
   * {@inheritdoc}
   */
  public function setLastFailureTime($time) {
    $this->data->lastFailureTime = $time;
  }

  /**
   * {@inheritdoc}
   */
  public function getLastFailureTime() {
    return $this->data->lastFailureTime;
  }

  /**
   * {@inheritdoc}
   */
  public function isBroken() {
    return $this->data->isBroken;
  }

  /**
   * {@inheritdoc}
   */
  public function setBroken($state) {
    $this->data->isBroken = $state;
  }

  /**
   * {@inheritdoc}
   */
  public function persist() {
    $this->data->lastUpdateTime = time();
    $this->cacheBackend->set($this->key, $this->data);
  }

}
