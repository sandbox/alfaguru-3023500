<?php

namespace Drupal\lastfm_field\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\lastfm_field\Services\LastfmClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 * @FieldFormatter(
 *   id = "lastfm_formatter",
 *   label = @Translation("Lastfm formatter"),
 *   field_types = {
 *     "lastfm_field"
 *   }
 * )
 */
class LastfmFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\lastfm_field\Services\LastfmClientInterface
   */
  protected $lastfmClient;

  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, LastfmClientInterface $lastfmClient) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->lastfmClient = $lastfmClient;
  }


  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = ['#markup' => $this->viewValue($item)];
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    $artist = $item->value;
    $info = $this->lastfmClient->lookupArtist($artist);
    $output = '';
    if (isset($info['name'])) {
      $output .= '<h2>' . $info['name'] . '</h2>';
    }
    if (isset($info['image']['extralarge'])) {
      $output .= '<img src="' . $info['image']['extralarge'] . '">';
    }
    if (isset($info['bio']['content'])) {
      $output .= '<p>' . nl2br($info['bio']['content']) . '</p>';
    }
    return $output;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('lastfm_field.lastfm_client')
    );
  }
}
