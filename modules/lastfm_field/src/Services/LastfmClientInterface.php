<?php

namespace Drupal\lastfm_field\Services;


interface LastfmClientInterface {

  /**
   * @param $artistName
   *
   * @return mixed
   */
  public function lookupArtist($artistName);

}