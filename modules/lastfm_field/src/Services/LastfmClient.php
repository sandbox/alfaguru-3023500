<?php

namespace Drupal\lastfm_field\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use LastFmApi\Api\ArtistApi;
use LastFmApi\Api\AuthApi;

class LastfmClient implements LastfmClientInterface {

  /**
   * @var ArtistApi
   */
  protected $artistApi;

  /**
   * LastfmClient constructor.
   *
   * @throws
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $config = $configFactory->get('lastfm_field.config');
    $api_key = $config->get('api_key');
    if ($api_key) {
      $auth = new AuthApi('setsession', array('apiKey' => $api_key));
      $this->artistApi = new ArtistApi($auth);
    }
  }

  /**
   * @param $artistName string
   *
   * @return mixed
   *
   * @throws \Exception
   */
  public function lookupArtist($artistName) {
    if ($this->artistApi) {
      return $this->artistApi->getInfo(['artist' => $artistName]);
    }
    else {
      return [];
    }
  }


}